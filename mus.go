package main

import (
	"fmt"
	"github.com/hajimehoshi/go-mp3"
	"github.com/hajimehoshi/oto"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	files, err := ioutil.ReadDir("/home/kaleb/Music")
	check(err)

	indx := rand.Intn(len(files) - 1)
	f, err := os.Open("/home/kaleb/Music/" + files[indx].Name())
	check(err)
	defer f.Close()
	d, err := mp3.NewDecoder(f)
	check(err)
	defer d.Close()
	p, err := oto.NewPlayer(d.SampleRate(), 2, 2, 8192)
	check(err)
	defer p.Close()

	fmt.Printf("Length: %d[bytes]\n", d.Length())

	if _, err := io.Copy(p, d); err != nil {
		panic(err)
	}
	d.Close()
	f.Close()

	for {
		indx := rand.Intn(len(files) - 1)
		f, err := os.Open("/home/kaleb/Music/" + files[indx].Name())
		check(err)
		defer f.Close()

		d, err := mp3.NewDecoder(f)
		check(err)
		defer d.Close()

		fmt.Printf("Length: %d[bytes]\n", d.Length())

		if _, err := io.Copy(p, d); err != nil {
			panic(err)
		}
		d.Close()
		f.Close()
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
