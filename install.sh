#!/bin/bash

#
# TO BE RUN FROM A FRESH INSTALL OF ARCH
# TO INSTALL THE DOTFILES USE `stow stow/*`
#

read -p 'username: ' user
read -p 'password: ' pass
useradd $user
passwd $user << EOF
$pass
$pass
EOF
mkdir /home/kaleb
chown kaleb /home/kaleb
pacman -S base-devel git go
pacman -D --asdeps go
echo "$user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers
GIT_DIR=$(pwd)
su - kaleb -c "
cd ~
mkdir aur
cd aur
curl -o yay.tar.gz https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz
tar -xf yay.tar.gz
ls
cd yay
makepkg -csi --noconfirm
cd
rm -rf aur
yay -c
yay -S fbterm tmux fish polkit terminus-font-ttf pulseaudio elinks neovim python-neovim python2-neovim stow go
stow stow/*
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
nvim +:PlugUpdate +:qall
"
gpasswd -a $user video
chown -R $user /home/kaleb
chsh $user << EOF
/usr/bin/fish
EOF
