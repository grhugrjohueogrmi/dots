import sys
import string

def generate(service):
    upper_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' ,'I', 'J'] 
    lower_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'] 
    passwd = ''
    last = 0
    for letter in service:
        ordl = string.ascii_letters.index(letter)
        
        Upper = (ordl-last)//10
        Lower = (ordl-last)%10
        passwd += upper_letters[Upper]
        passwd += lower_letters[Lower]
        passwd += str(ordl*last)
        
        last = ordl
    while len(passwd) < 32:
        
        ordl = len(passwd)
        
        Upper = (ordl-last)//10
        Lower = (ordl-last)%10
        passwd += upper_letters[Upper]
        passwd += lower_letters[Lower]
        passwd += str(ordl*last)
        
        last = ordl
    return passwd[0:32]

if __name__ == '__main__':
    print(generate(sys.argv[1]))
