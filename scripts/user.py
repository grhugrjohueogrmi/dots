import sys
import string

def generate(service):
    syla = ['ar', 'be', 'ca', 'du', 'ev', 'fa', 'gi', 'hm', 'ie', 'jo'] 
    passwd = ''
    last = 0
    for letter in service:
        ordl = string.ascii_letters.index(letter)
        
        Upper = (ordl-last)//10
        Lower = (ordl-last)%10
        passwd += syla[Upper]
        passwd += syla[Lower]
        
        last = ordl
    while len(passwd) < 8:
        
        ordl = len(passwd)
        
        Upper = (ordl-last)//10
        Lower = (ordl-last)%10
        passwd += syla[Upper]
        passwd += syla[Lower]
        
        last = ordl
    return passwd[0:32]

if __name__ == '__main__':
    print(generate(sys.argv[1]))
