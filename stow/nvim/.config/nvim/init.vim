call plug#begin()
Plug 'fatih/vim-go'
Plug 'majutsushi/tagbar'
Plug 'fatih/molokai'
Plug 'Valloric/YouCompleteMe'
Plug 'nsf/gocode', {'rtp': 'vim/'}
Plug 'tpope/vim-fugitive'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'ayu-theme/ayu-vim'
call plug#end()

filetype plugin indent on
"Ok. Done.

let g:molokai_original=1
set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0
let g:go_fmt_command = "gofmt"
set tabstop=2
set shiftwidth=2
set autoindent
set ignorecase
set smartcase
set relativenumber
set ruler
set autowrite
syntax on
set backspace=indent,eol,start
set ic
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-h>"
let g:UltiSnipsJumpForwardTrigger="<c-n>"
let g:UltiSnipsJumpBackwardTrigger="<c-m>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

"if exists ("g:did_load_filetypes")
" filetype off
" filetype plugin indent off
"endif
"set runtimepath+=/usr/local/go/misc/vim
"filetype plugin indent on

"Colors
highlight Pmenu ctermfg=0 ctermbg=8
highlight PmenuSel ctermfg=8 
highlight PmenuSbar ctermfg=8 ctermbg=8
highlight PmenuThumb ctermfg=7 ctermfg=7


highlight SpecialKey ctermfg=6
highlight NonText ctermfg=6

highlight Directory ctermfg=6
highlight ErrorMsg ctermfg=0 ctermbg=1
highlight Search ctermfg=0 ctermbg=3
highlight MoreMsg ctermfg=6

highlight LineNr ctermfg=0 ctermbg=8
highlight CursorLineNr ctermfg=3 ctermbg=8

highlight Question ctermfg=6
highlight StatusLine cterm=none ctermfg=0 ctermbg=8
highlight StatusLineNC ctermfg=8 
highlight VertSplit ctermfg=8 ctermbg=8

highlight Title ctermfg=0 ctermbg=8
highlight Visual ctermbg=8
highlight WarningMsg ctermfg=1

highlight WildMenu ctermfg=0 ctermbg=3
highlight Folded ctermfg=6 ctermfg=7
highlight FoldColumn ctermfg=6 ctermbg=7

highlight DiffAdd ctermfg=0 ctermbg=4
highlight DiffChange ctermfg=0 ctermbg=5
highlight DiffDelete ctermfg=0 ctermbg=1
highlight DiffText ctermfg=0 ctermbg=6

highlight SignColumn ctermfg=6 ctermbg=8
highlight Conceal ctermfg=6 ctermbg=8

highlight SpellBad ctermbg=1
highlight SpellCap ctermbg=6
highlight SpellRare ctermbg=6
highlight SpellLocal ctermbg=6

highlight TabLine ctermfg=8 ctermbg=7


highlight CursorColumn  ctermfg=7
highlight CursorLine  ctermfg=7

highlight ColorColumn  ctermfg=7

highlight QuickFixLine ctermfg=4 
highlight MatchParen ctermfg=0 ctermbg=1

highlight Delimiter  ctermfg=6
highlight Identifier ctermfg=4
highlight Type ctermfg=4
highlight Error ctermfg=1
highlight Comment ctermfg=2
highlight Constant ctermfg=5
highlight Special ctermfg=4
highlight Statment ctermfg=3
highlight PreProc ctermfg=3
highlight Underlined cterm=underline
highlight Ignore ctermfg=17
highlight Todo ctermfg=0 ctermbg=2


" Open file at a position where it was last left.
au BufWinLeave *.go mkview
au BufWinEnter *.go silent loadview

set so=999
